import 'package:clima_flutter/app/pages/home/home_bloc.dart';
import 'package:clima_flutter/app/pages/home/home_module.dart';
import 'package:clima_flutter/app/pages/location/location_bloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:clima_flutter/app/pages/location/location_page.dart';
import 'package:flutter/material.dart';

class LocationModule extends ModuleWidget {
  @override
  List<Bloc> get blocs => [
        Bloc((i) => LocationBloc(weatherData: HomeModule.to.bloc<HomeBloc>().data), singleton: false),
      ];

  @override
  List<Dependency> get dependencies => [];

  @override
  Widget get view => LocationPage();

  static Inject get to => Inject<LocationModule>.of();
}
