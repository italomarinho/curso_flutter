import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:clima_flutter/app/app_module.dart';
import 'package:clima_flutter/app/repository/weather_repository_repository.dart';
import 'package:clima_flutter/app/shared/location.dart';
import 'package:clima_flutter/app/shared/models/weather_coord_model.dart';

class HomeBloc extends BlocBase {
  Location _location;
  WeatherRepositoryRepository _repository;
  WeatherCoordModel data;

  // final _weatherController = BehaviorSubject<WeatherCoordModel>();
  // Stream<WeatherCoordModel> get outWeather => _weatherController.stream;
  // Sink<WeatherCoordModel> get intWeather => _weatherController.sink;

  HomeBloc(this._location) {
    _repository = AppModule.to.getDependency<WeatherRepositoryRepository>();
//    getLocationData();
  }

  Future<void> getLocationData() async {
    await _location.getCurrentLocation();

    data = await _repository.getDataCoordinates(
        latitude: _location.latitude, longitude: _location.longitude);

  }

  //dispose will be called automatically by closing its streams
  @override
  void dispose() {
    // _weatherController.close();
    _repository.dispose();
    super.dispose();
  }
}
