import 'package:clima_flutter/app/pages/home/home_bloc.dart';
import 'package:clima_flutter/app/pages/home/home_module.dart';
import 'package:clima_flutter/app/pages/location/location_module.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final bloc = HomeModule.to.bloc<HomeBloc>();

  @override
  void initState() {
    super.initState();

    getLocation();
  }

  void getLocation() async {
    await bloc.getLocationData();

    Navigator.push(
        context, CupertinoPageRoute(builder: (context) => LocationModule()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SpinKitWave(
          color: Colors.white,
          size: 90,
        ),
      ),
    );
  }
}
