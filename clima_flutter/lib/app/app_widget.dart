import 'package:flutter/material.dart';
import 'package:clima_flutter/app/pages/home/home_module.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Clima',
      theme: ThemeData.dark(),
      home: HomeModule(),
    );
  }
}
