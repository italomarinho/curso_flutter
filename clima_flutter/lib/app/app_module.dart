import 'package:clima_flutter/app/repository/weather_repository_repository.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:clima_flutter/app/app_bloc.dart';
import 'package:clima_flutter/app/app_widget.dart';
import 'package:clima_flutter/app/shared/custom_dio/custom_dio.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class AppModule extends ModuleWidget {
  @override
  List<Bloc> get blocs => [
        Bloc((i) => AppBloc()),
      ];

  @override
  List<Dependency> get dependencies => [
        Dependency((i) => Dio()),
        Dependency((i) => CustomDio(i.getDependency<Dio>())),
        Dependency((i) => WeatherRepositoryRepository(i.getDependency<CustomDio>()))
      ];

  @override
  Widget get view => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
