import 'address_model.dart';

class AdModel {
  List<dynamic> images;
  String title;
  String description;
  AddressModel address;
  num price;
  bool hidePhone;

  DateTime dateCreated = DateTime.now();

  AdModel();

  factory AdModel.fromJson(Map<String, dynamic> json) {
    return AdModel(
        //field: json[''],
        );
  }

  Map<String, dynamic> toJson() => {};

  @override
  String toString() {
    return '$images, $title, $description, $address, $price, $hidePhone';
  }
}
