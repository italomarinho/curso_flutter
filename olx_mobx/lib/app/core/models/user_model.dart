enum UserType { particular, professional }

class UserModel {
  UserModel({this.name, this.email, this.password, this.phone});

  String name;
  String email;
  String password;

  String phone;
  UserType userType = UserType.particular;
}
