import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../custom_drawer_controller.dart';
import 'icon_title.dart';

class IconSection extends StatelessWidget {
  final CustomDrawerController _controller = Modular.get();

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) {
        return Column(
          children: <Widget>[
            IconTitle(
              label: 'Anúncios',
              iconData: Icons.list,
              onTap: () => _controller.pushReplacementNamed('/', context),
              highlighted: _controller.router == '/',
            ),
            IconTitle(
              label: 'Inserir Anúncios',
              iconData: Icons.edit,
              onTap: () => _controller.pushReplacementNamed('/create', context),
              highlighted: _controller.router == '/create',
            ),
            IconTitle(
              label: 'Chat',
              iconData: Icons.chat,
              onTap: () {
                // _controller.setPage(2);
              },
              highlighted: false,
            ),
            IconTitle(
              label: 'Favoritos',
              iconData: Icons.favorite,
              onTap: () {
                // _controller.setPage(3);
              },
              highlighted: false,
            ),
            IconTitle(
              label: 'Minha conta',
              iconData: Icons.person,
              onTap: () =>
                  _controller.pushReplacementNamed('/account', context),
              highlighted: _controller.router == '/account',
            ),
          ],
        );
      },
    );
  }
}
