import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'custom_drawer_controller.g.dart';

class CustomDrawerController = _CustomDrawerControllerBase
    with _$CustomDrawerController;

abstract class _CustomDrawerControllerBase with Store {
  @observable
  String router = '/';

  @action
  void setRouter(String value) => router = value;

  void pushReplacementNamed(String router, BuildContext context) {
    // Pega a rota atual
    var actualRoute = ModalRoute.of(context).settings.name;
    if (actualRoute == router) return;

    setRouter(router);

    Modular.to.pop();
    Modular.to.pushReplacementNamed('$router');
  }
}
