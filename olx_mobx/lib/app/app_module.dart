import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'app_controller.dart';
import 'app_widget.dart';
import 'core/stores/ad_store.dart';
import 'core/widgets/common_drawer/custom_drawer_controller.dart';
import 'modules/account/account_module.dart';
import 'modules/create/create_module.dart';
import 'modules/home/home_module.dart';
import 'modules/login/login_module.dart';
import 'my_custom_transition.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind((i) => AppController()),
        Bind((i) => CustomDrawerController()),
        Bind((i) => Dio()),

        // Stores
        Bind((i) => AdStore()),
      ];

  @override
  List<Router> get routers => [
        Router(
          Modular.initialRoute,
          module: HomeModule(),
          transition: transition(),
          customTransition: customTransition(),
        ),
        Router(
          '/login',
          module: LoginModule(),
          transition: transition(),
          customTransition: customTransition(),
        ),
        Router(
          '/create',
          module: CreateModule(),
          transition: transition(),
          customTransition: customTransition(),
        ),
        Router(
          '/account',
          module: AccountModule(),
          transition: transition(),
          customTransition: customTransition(),
        ),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
