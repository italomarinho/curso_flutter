import 'package:flutter_modular/flutter_modular.dart';

import 'account_controller.dart';
import 'account_page.dart';
import 'pages/edit_account/edit_account_controller.dart';
import 'pages/edit_account/edit_account_page.dart';

class AccountModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => EditAccountController()),
        Bind((i) => AccountController()),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, child: (_, args) => AccountPage()),
        Router('/editAccount', child: (_, args) => EditAccountPage()),
      ];

  static Inject get to => Inject<AccountModule>.of();
}
