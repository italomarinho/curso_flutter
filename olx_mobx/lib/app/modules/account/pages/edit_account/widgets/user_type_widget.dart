import 'package:flutter/material.dart';

import '../../../../../core/models/user_model.dart';

class UserTypeWidget extends StatelessWidget {
  final UserType initialValue;
  final FormFieldSetter<UserType> onSaved;

  const UserTypeWidget({Key key, this.initialValue, this.onSaved})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FormField(
        initialValue: initialValue,
        onSaved: onSaved,
        builder: (state) {
          return Wrap(
            alignment: WrapAlignment.spaceEvenly,
            children: <Widget>[
              Column(
                children: <Widget>[
                  const Text(
                    'Particular',
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                  ),
                  Radio(
                    value: UserType.particular,
                    groupValue: state.value,
                    onChanged: state.didChange,
                  )
                ],
              ),
              Column(
                children: <Widget>[
                  const Text(
                    'Profissional',
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                  ),
                  Radio(
                    value: UserType.professional,
                    groupValue: state.value,
                    onChanged: state.didChange,
                  )
                ],
              )
            ],
          );
        });
  }
}
