import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../core/helpers/format_field.dart';
import '../home_controller.dart';

class ProductTile extends StatelessWidget {
  final int id;

  const ProductTile({Key key, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var controller = Modular.get<HomeController>();
    var adModel = controller.adStore.ads[id];

    return InkWell(
      onTap: () => Modular.to.pushNamed('/product', arguments: adModel),
      child: Container(
        height: 135,
        margin: const EdgeInsets.symmetric(horizontal: 2, vertical: 1),
        child: Card(
          child: Row(
            children: <Widget>[
              SizedBox(
                height: 135,
                width: 127,
                child: Image.file(
                  adModel.images[0],
                  fit: BoxFit.cover,
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(12),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        adModel.title,
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      Text(
                        'R\$ ${numToString(adModel.price)}',
                        style: TextStyle(
                            fontSize: 19, fontWeight: FontWeight.w900),
                      ),
                      Text(
                        '${adModel.address.bairro}, '
                        '${adModel.address.localidade}',
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
