import 'package:flutter_modular/flutter_modular.dart';

import '../../core/stores/ad_store.dart';
import 'home_controller.dart';
import 'home_page.dart';
import 'pages/filter/filter_controller.dart';
import 'pages/filter/filter_page.dart';
import 'pages/product/product_controller.dart';
import 'pages/product/product_page.dart';

class HomeModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => ProductController()),
        Bind((i) => FilterController()),
        Bind((i) => HomeController(i.get<AdStore>())),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, child: (_, args) => HomePage()),
        Router('/filter', child: (_, args) => FilterPage()),
        Router("/product", child: (_, args) => ProductPage(adModel: args.data)),
      ];

  static Inject get to => Inject<HomeModule>.of();
}
