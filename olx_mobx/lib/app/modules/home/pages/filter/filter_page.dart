import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../core/models/filter_model.dart';
import 'filter_controller.dart';
import 'widigets/animated_button.dart';
import 'widigets/order_by_filter.dart';
import 'widigets/price_range_field.dart';
import 'widigets/section_title.dart';
import 'widigets/vendor_type_field.dart';

class FilterPage extends StatefulWidget {
  final String title;
  const FilterPage({Key key, this.title = "Filter"}) : super(key: key);

  @override
  _FilterPageState createState() => _FilterPageState();
}

class _FilterPageState extends ModularState<FilterPage, FilterController> {
  //use 'controller' variable to access controller

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final ScrollController _scrollController = ScrollController();

  final FilterModel _filter = FilterModel(
      maxPrice: 100,
      minPrice: 10,
      vendorType: vendorTypeParticular,
      orderBy: OrderBy.price);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        elevation: 0,
        title: Text('Filtrar busca'),
      ),
      body: Stack(
        children: <Widget>[
          Form(
            key: _formKey,
            child: ListView(
              controller: _scrollController,
              padding: const EdgeInsets.all(16),
              children: <Widget>[
                const SectionTitle(title: 'Ordenar por'),
                OrderByField(
                  initialValue: _filter.orderBy,
                  onSaved: (v) {
                    _filter.orderBy = v;
                  },
                ),
                const SectionTitle(title: 'Preço (R\$)'),
                PriceRangeField(
                  filter: _filter,
                ),
                const SectionTitle(title: 'Tipo de anunciante'),
                VendorTypeField(
                  initialValue: _filter.vendorType,
                  onSaved: (v) {
                    _filter.vendorType = v;
                  },
                ),
                const SizedBox(height: 300)
              ],
            ),
          ),
          AnimatedButton(
            scrollController: _scrollController,
            onTap: () {
              if (_formKey.currentState.validate()) {
                _formKey.currentState.save();

                if (_filter.maxPrice != null && _filter.minPrice != null) {
                  if (_filter.minPrice > _filter.maxPrice) {
                    _scaffoldKey.currentState.showSnackBar(
                      SnackBar(
                        content: const Text('Faixa de preço inválida'),
                        backgroundColor: Colors.pink,
                      ),
                    );
                    return;
                  }
                }
                //! Salvar tudo e pesquisar anuncios
              }
            },
          ),
        ],
      ),
    );
  }
}
