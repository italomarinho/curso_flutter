import 'package:flutter/material.dart';

import '../../../../../core/models/filter_model.dart';

class OrderByField extends StatelessWidget {
  final FormFieldSetter<OrderBy> onSaved;
  final OrderBy initialValue;

  const OrderByField({Key key, this.onSaved, this.initialValue})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FormField<OrderBy>(
      initialValue: initialValue,
      onSaved: onSaved,
      builder: (state) {
        return Row(
          children: <Widget>[
            GestureDetector(
              onTap: () {
                state.didChange(OrderBy.date);
              },
              child: Container(
                height: 50,
                width: 80,
                decoration: BoxDecoration(
                    border: Border.all(
                        color: state.value == OrderBy.date
                            ? Colors.transparent
                            : Colors.grey),
                    borderRadius: const BorderRadius.all(Radius.circular(50)),
                    color: state.value == OrderBy.date
                        ? Colors.blue
                        : Colors.transparent),
                alignment: Alignment.center,
                child: Text(
                  'Data',
                  style: TextStyle(
                      color: state.value == OrderBy.date
                          ? Colors.white
                          : Colors.black),
                ),
              ),
            ),
            const SizedBox(width: 10),
            GestureDetector(
              onTap: () {
                state.didChange(OrderBy.price);
              },
              child: Container(
                height: 50,
                width: 80,
                decoration: BoxDecoration(
                    border: Border.all(
                        color: state.value == OrderBy.price
                            ? Colors.transparent
                            : Colors.grey),
                    borderRadius: const BorderRadius.all(Radius.circular(50)),
                    color: state.value == OrderBy.price
                        ? Colors.blue
                        : Colors.transparent),
                alignment: Alignment.center,
                child: Text(
                  'Preço',
                  style: TextStyle(
                      color: state.value == OrderBy.price
                          ? Colors.white
                          : Colors.black),
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
