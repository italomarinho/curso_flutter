import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';

import '../../../../core/models/ad_model.dart';
import 'widgets/bottom_bar.dart';
import 'widgets/description_panel.dart';
import 'widgets/location_panel.dart';
import 'widgets/main_panel.dart';
import 'widgets/user_panel.dart';

class ProductPage extends StatelessWidget {
  final AdModel adModel;

  const ProductPage({Key key, this.adModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Anúncio'),
        elevation: 0,
      ),
      body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              Container(
                height: 280,
                child: Carousel(
                  images: adModel.images.map((e) => FileImage(e)).toList(),
                  dotSize: 4,
                  dotSpacing: 15,
                  dotBgColor: Colors.transparent,
                  dotColor: Colors.pink,
                  autoplay: false,
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    MainPanel(adModel),
                    Divider(),
                    DescriptionPanel(adModel),
                    Divider(),
                    LocationPanel(adModel),
                    Divider(),
                    UserPanel(adModel),
                    const SizedBox(height: 32)
                  ],
                ),
              )
            ],
          ),
          BottomBar(adModel),
        ],
      ),
    );
  }
}
