// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$LoginController on _LoginControllerBase, Store {
  Computed<bool> _$isEmailValidComputed;

  @override
  bool get isEmailValid =>
      (_$isEmailValidComputed ??= Computed<bool>(() => super.isEmailValid))
          .value;
  Computed<bool> _$isPasswordValidComputed;

  @override
  bool get isPasswordValid => (_$isPasswordValidComputed ??=
          Computed<bool>(() => super.isPasswordValid))
      .value;
  Computed<bool> _$isFormValidComputed;

  @override
  bool get isFormValid =>
      (_$isFormValidComputed ??= Computed<bool>(() => super.isFormValid)).value;
  Computed<String> _$emailErrorComputed;

  @override
  String get emailError =>
      (_$emailErrorComputed ??= Computed<String>(() => super.emailError)).value;
  Computed<String> _$passwordErrorComputed;

  @override
  String get passwordError =>
      (_$passwordErrorComputed ??= Computed<String>(() => super.passwordError))
          .value;

  final _$emailAtom = Atom(name: '_LoginControllerBase.email');

  @override
  String get email {
    _$emailAtom.context.enforceReadPolicy(_$emailAtom);
    _$emailAtom.reportObserved();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.context.conditionallyRunInAction(() {
      super.email = value;
      _$emailAtom.reportChanged();
    }, _$emailAtom, name: '${_$emailAtom.name}_set');
  }

  final _$passwordAtom = Atom(name: '_LoginControllerBase.password');

  @override
  String get password {
    _$passwordAtom.context.enforceReadPolicy(_$passwordAtom);
    _$passwordAtom.reportObserved();
    return super.password;
  }

  @override
  set password(String value) {
    _$passwordAtom.context.conditionallyRunInAction(() {
      super.password = value;
      _$passwordAtom.reportChanged();
    }, _$passwordAtom, name: '${_$passwordAtom.name}_set');
  }

  final _$loadingAtom = Atom(name: '_LoginControllerBase.loading');

  @override
  bool get loading {
    _$loadingAtom.context.enforceReadPolicy(_$loadingAtom);
    _$loadingAtom.reportObserved();
    return super.loading;
  }

  @override
  set loading(bool value) {
    _$loadingAtom.context.conditionallyRunInAction(() {
      super.loading = value;
      _$loadingAtom.reportChanged();
    }, _$loadingAtom, name: '${_$loadingAtom.name}_set');
  }

  final _$loggedAtom = Atom(name: '_LoginControllerBase.logged');

  @override
  bool get logged {
    _$loggedAtom.context.enforceReadPolicy(_$loggedAtom);
    _$loggedAtom.reportObserved();
    return super.logged;
  }

  @override
  set logged(bool value) {
    _$loggedAtom.context.conditionallyRunInAction(() {
      super.logged = value;
      _$loggedAtom.reportChanged();
    }, _$loggedAtom, name: '${_$loggedAtom.name}_set');
  }

  final _$loggedFacebookAtom =
      Atom(name: '_LoginControllerBase.loggedFacebook');

  @override
  bool get loggedFacebook {
    _$loggedFacebookAtom.context.enforceReadPolicy(_$loggedFacebookAtom);
    _$loggedFacebookAtom.reportObserved();
    return super.loggedFacebook;
  }

  @override
  set loggedFacebook(bool value) {
    _$loggedFacebookAtom.context.conditionallyRunInAction(() {
      super.loggedFacebook = value;
      _$loggedFacebookAtom.reportChanged();
    }, _$loggedFacebookAtom, name: '${_$loggedFacebookAtom.name}_set');
  }

  final _$_LoginControllerBaseActionController =
      ActionController(name: '_LoginControllerBase');

  @override
  void setEmail(String value) {
    final _$actionInfo = _$_LoginControllerBaseActionController.startAction();
    try {
      return super.setEmail(value);
    } finally {
      _$_LoginControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setPassword(String value) {
    final _$actionInfo = _$_LoginControllerBaseActionController.startAction();
    try {
      return super.setPassword(value);
    } finally {
      _$_LoginControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setLoading({bool value}) {
    final _$actionInfo = _$_LoginControllerBaseActionController.startAction();
    try {
      return super.setLoading(value: value);
    } finally {
      _$_LoginControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setLogged({bool value}) {
    final _$actionInfo = _$_LoginControllerBaseActionController.startAction();
    try {
      return super.setLogged(value: value);
    } finally {
      _$_LoginControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setLoginFacebook({bool value}) {
    final _$actionInfo = _$_LoginControllerBaseActionController.startAction();
    try {
      return super.setLoginFacebook(value: value);
    } finally {
      _$_LoginControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string =
        'email: ${email.toString()},password: ${password.toString()},loading: ${loading.toString()},logged: ${logged.toString()},loggedFacebook: ${loggedFacebook.toString()},isEmailValid: ${isEmailValid.toString()},isPasswordValid: ${isPasswordValid.toString()},isFormValid: ${isFormValid.toString()},emailError: ${emailError.toString()},passwordError: ${passwordError.toString()}';
    return '{$string}';
  }
}
