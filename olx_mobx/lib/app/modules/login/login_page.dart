import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'login_controller.dart';
import 'widgets/facebook_button.dart';
import 'widgets/login_button.dart';
import 'widgets/or_divider.dart';

class LoginPage extends StatefulWidget {
  final String title;
  const LoginPage({Key key, this.title = "Entrar"}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends ModularState<LoginPage, LoginController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          centerTitle: false,
          elevation: 0,
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                FacebookButton(),
                OrDivider(),
                Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 11),
                  child: Text(
                    'Acessar com e-mail:',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 16, color: Colors.grey[900]),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 3, bottom: 4),
                  child: Text(
                    'E-mail:',
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.grey[800],
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
                Observer(builder: (_) {
                  return TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    autocorrect: false,
                    decoration: InputDecoration(
                      border: const OutlineInputBorder(),
                      errorText: controller.emailError,
                    ),
                    onChanged: controller.setEmail,
                    enabled: !controller.loading,
                  );
                }),
                Padding(
                  padding: const EdgeInsets.only(left: 3, bottom: 4, top: 20),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          'Senha',
                          style: TextStyle(
                            color: Colors.grey[800],
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      InkWell(
                        child: Text(
                          'Esqueceu sua senha?',
                          style: TextStyle(
                            color: Colors.blue,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                        onTap: () {},
                      )
                    ],
                  ),
                ),
                Observer(builder: (_) {
                  return TextFormField(
                    autocorrect: false,
                    obscureText: true,
                    decoration: InputDecoration(
                      border: const OutlineInputBorder(),
                      errorText: controller?.passwordError,
                    ),
                    onChanged: controller.setPassword,
                    enabled: !controller.loading,
                  );
                }),
                LoginButton(),
                Divider(color: Colors.grey),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 12),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      const Text(
                        'Não tem uma conta? ',
                        style: TextStyle(fontSize: 16),
                      ),
                      InkWell(
                        onTap: () => Modular.link.pushNamed('/signup'),
                        child: const Text(
                          'Cadastre-se',
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                            color: Colors.blue,
                            fontSize: 16,
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
