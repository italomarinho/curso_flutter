import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:oktoast/oktoast.dart';

import '../../core/helpers/validators.dart';

part 'login_controller.g.dart';

class LoginController = _LoginControllerBase with _$LoginController;

abstract class _LoginControllerBase with Store {
  @observable
  String email;

  @observable
  String password;

  @observable
  bool loading = false;

  @observable
  bool logged = false;

  @observable
  bool loggedFacebook = false;

  @action
  void setEmail(String value) => email = value;

  @action
  void setPassword(String value) => password = value;

  @action
  void setLoading({bool value}) => loading = value;

  @action
  void setLogged({bool value}) => logged = value;

  @action
  void setLoginFacebook({bool value}) => loggedFacebook = value;

  @computed
  bool get isEmailValid {
    if (email != null) {
      return validEmail(email);
    } else {
      return false;
    }
  }

  @computed
  bool get isPasswordValid {
    if (password.isEmpty) {
      return false;
    } else {
      return true;
    }
  }

  @computed
  bool get isFormValid {
    if (isEmailValid && isPasswordValid) {
      return true;
    } else {
      return false;
    }
  }

  @computed
  String get emailError {
    if (email == null) {
      return null;
    } else if (email.isEmpty) {
      return 'Campo obrigatório';
    } else if (!isEmailValid) {
      return 'E-mail inválido';
    } else {
      return null;
    }
  }

  @computed
  String get passwordError {
    if (password == null) {
      return null;
    } else if (!isPasswordValid) {
      return 'Campo obrigatório';
    } else {
      return null;
    }
  }

  Future<void> loginWithEmail() async {
    setLoading(value: true);
    if (isFormValid) {
      // Codigo de login

      // Se login efetudo com sucesso
      setLogged(value: true);

      await Future.delayed(Duration(seconds: 2));

      if (logged) {
        Modular.to.pop();
      }
    } else {
      showToast('Insira seu e-mail e senha');
    }

    setLoading(value: false);
  }

  Future<void> loginWithFacebook() async {
    setLoginFacebook(value: true);

    // Se login efetudo com sucesso
    setLogged(value: true);

    await Future.delayed(Duration(seconds: 2));

    if (logged) {
      Modular.to.pop();
    }
    setLoginFacebook(value: false);
  }
}
