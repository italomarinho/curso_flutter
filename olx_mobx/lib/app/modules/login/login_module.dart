import 'package:flutter_modular/flutter_modular.dart';

import 'login_controller.dart';
import 'login_page.dart';
import 'pages/signup/signup_controller.dart';
import 'pages/signup/signup_page.dart';

class LoginModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => SignUpController(), singleton: false),
        Bind((i) => LoginController(), singleton: false),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, child: (_, args) => LoginPage()),
        Router('/signup', child: (context, args) => SignUpPage()),
      ];

  static Inject get to => Inject<LoginModule>.of();
}
