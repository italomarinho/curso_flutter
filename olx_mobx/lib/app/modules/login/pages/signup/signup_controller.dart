import 'package:mobx/mobx.dart';

import '../../../../core/models/user_model.dart';

part 'signup_controller.g.dart';

enum SignUpState { idle, loading, error }

class SignUpController = _SignUpControllerBase with _$SignUpController;

abstract class _SignUpControllerBase with Store {
  @observable
  UserModel user = UserModel();

  @observable
  SignUpState state = SignUpState.idle;

  @action
  void setState(SignUpState value) => state = value;

  @action
  void setName(String name) => user.name = name;

  @action
  void setEmail(String email) => user.email = email;

  @action
  void setPassword(String password) => user.password = password;

  Future<void> signUp() async {
    setState(SignUpState.loading);

    await Future.delayed(Duration(seconds: 2));

    setState(SignUpState.idle);
  }
}
