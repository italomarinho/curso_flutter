import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../core/stores/ad_store.dart';
import '../../repository/cep_repository.dart';
import 'create_controller.dart';
import 'create_page.dart';
import 'widgets/cep_field/cep_field_controller.dart';

class CreateModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => CepRepository(i.get<Dio>())),
        Bind((i) => CreateController(i.get<AdStore>())),
        Bind((i) => CepFieldController(i.get<CepRepository>())),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, child: (_, args) => CreatePage()),
      ];

  static Inject get to => Inject<CreateModule>.of();
}
