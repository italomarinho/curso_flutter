import 'package:brasil_fields/brasil_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../core/models/address_model.dart';
import 'cep_field_controller.dart';

class CepField extends StatefulWidget {
  final InputDecoration decoration;
  final FormFieldSetter<AddressModel> onSaved;

  const CepField({Key key, this.decoration, this.onSaved}) : super(key: key);
  @override
  _CepFieldState createState() => _CepFieldState();
}

class _CepFieldState extends ModularState<CepField, CepFieldController> {
  InputDecoration get decoration => widget.decoration;
  FormFieldSetter<AddressModel> get onSaved => widget.onSaved;

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) {
        if (controller.state == CepFieldState.initializing) return Container();

        return Column(
          children: <Widget>[
            TextFormField(
              initialValue: controller.cep,
              keyboardType: TextInputType.number,
              decoration: decoration,
              inputFormatters: [
                WhitelistingTextInputFormatter.digitsOnly,
                CepInputFormatter(),
              ],
              onSaved: (cep) {
                onSaved(controller.addressModel);
              },
              onChanged: controller.onChanged,
              validator: (c) {
                switch (controller.state) {
                  case CepFieldState.initializing:
                  case CepFieldState.incomplete:
                  case CepFieldState.invalid:
                    return 'Campo obrigatório';
                  case CepFieldState.valid:
                }
                return null;
              },
            ),
            buildInformation(controller.state),
          ],
        );
      },
    );
  }

  Widget buildInformation(CepFieldState state) {
    switch (state) {
      case CepFieldState.initializing:
      case CepFieldState.incomplete:
        return Container();
      case CepFieldState.invalid:
        return Container(
          height: 50,
          padding: const EdgeInsets.all(8),
          color: Colors.red.withAlpha(100),
          alignment: Alignment.center,
          child: Text(
            'CEP inválido',
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.w600, color: Colors.red),
          ),
        );
      case CepFieldState.valid:
        final _address = controller.addressModel;
        return Container(
          height: 50,
          padding: const EdgeInsets.all(8),
          color: Colors.pink,
          alignment: Alignment.center,
          child: Text(
            '''Localização: ${_address.bairro} - 
          ${_address.localidade} - ${_address.uf}''',
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.w600, color: Colors.white),
          ),
        );
    }
    return Container();
  }
}
