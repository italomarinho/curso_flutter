import 'package:mobx/mobx.dart';

import '../../../../core/models/address_model.dart';
import '../../../../repository/cep_repository.dart';

part 'cep_field_controller.g.dart';

enum CepFieldState { initializing, incomplete, invalid, valid }

class CepFieldController = _CepFieldControllerBase with _$CepFieldController;

abstract class _CepFieldControllerBase with Store {
  final CepRepository repository;

  _CepFieldControllerBase(this.repository) {
    onChanged('');
  }

  @observable
  CepFieldState state = CepFieldState.initializing;

  @observable
  String cep;

  @observable
  AddressModel addressModel;

  @action
  void setState(CepFieldState value) => state = value;

  @action
  void setCep(String value) => cep = value;

  @action
  void setAddressModel(AddressModel value) => addressModel = value;

  void searchCep(String cep) async {
    final response = await repository.fetchCep(cep);

    if (response.success) {
      setState(CepFieldState.valid);
      setCep(cep);
      setAddressModel(response.result);
    } else {
      setState(CepFieldState.invalid);
      setCep(cep);
    }
  }

  void onChanged(String cep) {
    cep = cep
        .trim()
        .replaceAll('.', '')
        .replaceAll('-', ''); // Remove espaços em brancos, '-' e ','

    if (cep.isEmpty || cep.length < 8) {
      setState(CepFieldState.incomplete);
      setCep(cep);
    } else {
      searchCep(cep);
    }
  }
}
