import 'package:flutter/material.dart';

const kLabel_text_style = TextStyle(fontSize: 18, color: kInactive_color);
const kNumber_text_style = TextStyle(fontSize: 50, fontWeight: FontWeight.w900);
const kLargeButtonTextStyle = TextStyle(fontSize: 25, fontWeight: FontWeight.bold);
const kTitleTextStyle = TextStyle(fontSize: 50, fontWeight: FontWeight.bold);
const kResultTextStyle = TextStyle(fontSize: 22, fontWeight: FontWeight.bold, color: kColorGreen);
const kBMITextStyle = TextStyle(fontSize: 100, fontWeight: FontWeight.bold);
const kBodyTextStyle = TextStyle(fontSize: 22);

// Colors
const kPrimary_color = Color(0xFF0A0E21);
const kInactive_color = Color(0xFF8D8E98);
const kInactive_card_color = Color(0xFF111328);
const kActiveCardColor = Color(0xFF1D1E33);
const kColorPink = Color(0xFFEB1555);
const kColorGreen = Color(0xFF24d876);