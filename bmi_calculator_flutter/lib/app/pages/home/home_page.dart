import 'package:bmi_calculator_flutter/app/pages/home/home_bloc.dart';
import 'package:bmi_calculator_flutter/app/pages/home/home_module.dart';
import 'package:bmi_calculator_flutter/app/pages/result/result_module.dart';
import 'package:bmi_calculator_flutter/app/shared/constants.dart';
import 'package:bmi_calculator_flutter/app/widigets/bottom_button.dart';
import 'package:bmi_calculator_flutter/app/widigets/icon_content.dart';
import 'package:bmi_calculator_flutter/app/widigets/reusable_card.dart';
import 'package:bmi_calculator_flutter/app/widigets/round_icon_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = HomeModule.to.bloc<HomeBloc>();

    return Scaffold(
      appBar: AppBar(
        title: Text('BMI CALCULATOR'),
        centerTitle: true,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: StreamBuilder<Gender>(
                      stream: bloc.outGender,
                      builder: (context, snapshot) {
                        return ReusableCard(
                          color:
                              snapshot.data == Gender.MALE ? kActiveCardColor : kInactive_card_color,
                          onTap: () {
                            bloc.genderSelected(Gender.MALE);
                          },
                          cardChild: IconContent(
                            icon: FontAwesomeIcons.mars,
                            label: 'MALE',
                          ),
                        );
                      }),
                ),
                Expanded(
                  child: StreamBuilder<Gender>(
                      stream: bloc.outGender,
                      builder: (context, snapshot) {
                        return ReusableCard(
                          color: snapshot.data == Gender.FEMALE
                              ? kActiveCardColor
                              : kInactive_card_color,
                          onTap: () {
                            bloc.genderSelected(Gender.FEMALE);
                          },
                          cardChild: IconContent(
                            icon: FontAwesomeIcons.venus,
                            label: 'FEMALE',
                          ),
                        );
                      }),
                ),
              ],
            ),
          ),
          Expanded(
            child: ReusableCard(
              color: kActiveCardColor,
              cardChild: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'HEIGHT',
                    style: kLabel_text_style,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.baseline,
                    textBaseline: TextBaseline.alphabetic,
                    children: <Widget>[
                      StreamBuilder<double>(
                          stream: bloc.outSlider,
                          initialData: bloc.height.toDouble(),
                          builder: (context, snapshot) {
                            return Text(
                              '${snapshot.data.toInt()}',
                              style: kNumber_text_style,
                            );
                          }),
                      Text(
                        'cm',
                        style: kLabel_text_style,
                      )
                    ],
                  ),
                  StreamBuilder<double>(
                      stream: bloc.outSlider,
                      initialData: bloc.height.toDouble(),
                      builder: (context, snapshot) {
                        return Slider(
                          value: snapshot.data,
                          min: 120.0,
                          max: 220.0,
                          onChanged: (double value) => bloc.heightSlider(value),
                        );
                      }),
                ],
              ),
            ),
          ),
          Expanded(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: ReusableCard(
                    color: kActiveCardColor,
                    cardChild: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'WEIGHT',
                          style: kLabel_text_style,
                        ),
                        StreamBuilder<int>(
                            stream: bloc.outWeight,
                            initialData: bloc.weight,
                            builder: (context, snapshot) {
                              return Text(
                                '${snapshot.data}',
                                style: kNumber_text_style,
                              );
                            }),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            RoundIconButton(
                              icon: FontAwesomeIcons.minus,
                              onPress: bloc.weightMinus,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            RoundIconButton(
                              icon: FontAwesomeIcons.plus,
                              onPress: bloc.weightPlus,
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: ReusableCard(
                    color: kActiveCardColor,
                    cardChild: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'AGE',
                          style: kLabel_text_style,
                        ),
                        StreamBuilder<int>(
                            stream: bloc.outAge,
                            initialData: bloc.age,
                            builder: (context, snapshot) {
                              return Text(
                                '${snapshot.data}',
                                style: kNumber_text_style,
                              );
                            }),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            RoundIconButton(
                              icon: FontAwesomeIcons.minus,
                              onPress: bloc.ageMinus,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            RoundIconButton(
                              icon: FontAwesomeIcons.plus,
                              onPress: bloc.agePlus,
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          BottomButton(
            buttonTitle: 'CALCULATE',
            onTap: () {
              Navigator.push(
                context,
                CupertinoPageRoute(
                  builder: (context) => ResultModule(),
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
