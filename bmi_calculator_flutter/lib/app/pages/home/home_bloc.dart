import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';

enum Gender { MALE, FEMALE }

class HomeBloc extends BlocBase {

  // Valores padrões
  int height = 172;
  int weight = 78;
  int age = 28;

  Gender selectedGender;

  final _genderController = BehaviorSubject<Gender>.seeded(Gender.MALE);
  Stream<Gender> get outGender => _genderController.stream;

  final _sliderController = BehaviorSubject<double>();
  Stream<double> get outSlider => _sliderController.stream;

  final _weightController = BehaviorSubject<int>();
  Stream<int> get outWeight => _weightController.stream;

  final _ageController = BehaviorSubject<int>();
  Stream<int> get outAge => _ageController.stream;


  void genderSelected(Gender gender){
    selectedGender = gender;
    _genderController.add(selectedGender);
  }

  void heightSlider(double value){
    height = value.round();
    _sliderController.add(value);
  }

  void weightMinus(){
    weight--;
    _weightController.add(weight);
  }

  void weightPlus(){
    weight++;
    _weightController.add(weight);
  }

  void ageMinus(){
    age--;
    _ageController.add(age);
  }
  void agePlus(){
    age++;
    _ageController.add(age);
  }

  //dispose will be called automatically by closing its streams
  @override
  void dispose() {
    _genderController.close();
    _sliderController.close();
    _weightController.close();
    _ageController.close();

    super.dispose();
  }
}
