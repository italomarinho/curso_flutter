import 'package:bmi_calculator_flutter/app/pages/home/home_bloc.dart';
import 'package:bmi_calculator_flutter/app/pages/home/home_module.dart';
import 'package:bmi_calculator_flutter/app/pages/result/result_bloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:bmi_calculator_flutter/app/pages/result/result_page.dart';
import 'package:flutter/material.dart';

class ResultModule extends ModuleWidget {
  @override
  List<Bloc> get blocs => [
        Bloc(
          (i) => ResultBloc(
            height: HomeModule.to.bloc<HomeBloc>().height,
            weight: HomeModule.to.bloc<HomeBloc>().weight,
          ),
          singleton: false,
        ),
      ];

  @override
  List<Dependency> get dependencies => [];

  @override
  Widget get view => ResultPage();

  static Inject get to => Inject<ResultModule>.of();
}
