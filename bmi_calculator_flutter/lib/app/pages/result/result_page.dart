import 'package:bmi_calculator_flutter/app/pages/result/result_bloc.dart';
import 'package:bmi_calculator_flutter/app/pages/result/result_module.dart';
import 'package:bmi_calculator_flutter/app/shared/constants.dart';
import 'package:bmi_calculator_flutter/app/widigets/bottom_button.dart';
import 'package:bmi_calculator_flutter/app/widigets/reusable_card.dart';
import 'package:flutter/material.dart';

class ResultPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = ResultModule.to.bloc<ResultBloc>();

    return Scaffold(
      appBar: AppBar(
        title: Text('BMI CALCULATOR'),
        centerTitle: true,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Container(
              padding: EdgeInsets.all(15),
              alignment: Alignment.bottomLeft,
              child: Text(
                'Your Result',
                style: kTitleTextStyle,
              ),
            ),
          ),
          Expanded(
            flex: 5,
            child: ReusableCard(
              color: kActiveCardColor,
              cardChild: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  StreamBuilder<String>(
                      stream: bloc.outResult,
                      initialData: '',
                      builder: (context, snapshot) {
                        return Text(
                          snapshot.data,
                          style: kResultTextStyle,
                        );
                      }),
                  StreamBuilder<String>(
                      stream: bloc.outBmi,
                      initialData: '',
                      builder: (context, snapshot) {
                        return Text(
                          snapshot.data,
                          style: kBMITextStyle,
                        );
                      }),
                  StreamBuilder<String>(
                      stream: bloc.outInterpretation,
                      initialData: '',
                      builder: (context, snapshot) {
                        return Text(
                          snapshot.data,
                          textAlign: TextAlign.center,
                          style: kBodyTextStyle,
                        );
                      })
                ],
              ),
            ),
          ),
          BottomButton(
            buttonTitle: 'RE-CALCULATE',
            onTap: () {
              Navigator.pop(context);
            },
          )
        ],
      ),
    );
  }
}
